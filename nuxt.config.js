module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'auto_vue',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: ['axios', 'bootstrap-vue']
  },
  plugins: [
      '~/plugins/vuejs-paginate',
      '~/plugins/vuejs-cookie',
      '~/plugins/vuejs-mask'
  ],
  modules: [
      'bootstrap-vue/nuxt',
      '@nuxtjs/sitemap'
  ],
  /*
  sitemap: {
    xmlNs: 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"',
    hostname: 'https://zapchasti-express.ru/',
    path: '/sitemap.xml',
    gzip: true,
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date()
    },
  },
   */
  css: [
    {
      src: '~assets/scss/main.sass',
      lang: 'sass'
    }
  ]
}

