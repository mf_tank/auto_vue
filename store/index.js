import Vue from 'vue';
import Vuex from 'vuex';
import api from '~/assets/js/api.js';

Vue.use(Vuex);

const store = () => new Vuex.Store({

  state: {
    brands: [],
    models: [],
    modifications: [],
    sections: [],
    products : [],

    brand: null,
    model: null,
    modification: null,
    section: null,
    product: null,

    productCount: 0,

    partsCategories: [],
    expandedSectionIds: [],
    mobileMenuOpened: false,

    carInfo: null,
    isProductCart: false
  },
  mutations: {
    setBrands(state, array) {
      state.brands = array;
    },
    setModels(state, array) {
      state.models = array;
    },
    setModifications(state, array) {
      state.modifications = array;
    },
    setSections(state, sections) {
      state.sections = sections;
    },
    setProducts(state, products) {
      state.products = products;
    },

    setBrand(state, brand) {
      state.brand = brand;
    },
    setModel(state, model) {
      state.model = model;
    },
    setModification(state, modification) {
      state.modification = modification;
    },
    setSection(state, section) {
      state.section = section;
    },
    setProduct(state, product) {
      state.product = product;
    },


    setPartsCategories(state, array) {
      state.partsCategories = array;
    },
    setExpandedSectionIds(state, expandedSectionIds) {
      state.expandedSectionIds = expandedSectionIds;
    },

    setProductCount(state, productCount) {
      state.productCount = productCount;
    },
    setCarInfo(state, carInfo) {
        state.carInfo = carInfo;
    },
    setMobileMenuOpened(state, opened) {
      state.mobileMenuOpened = opened;
    },
    setIsProductCart(state, isProductCart) {
        state.isProductCart = isProductCart;
    }
  },
    actions: {
        getBrands({ commit, state }) {
            return api.getBrands().then(response => {

                commit("setBrands", response);

            });
        },
        getModifications({ commit, state }, object) {
            return api.getModifications(object.brandDescription, object.modelDescription)
                .then(response => {
                    commit('setModifications', response);
                });
        },
        getModels({ commit, state }, brandDescription) {
            return api.getModels(brandDescription).then(response => {

                commit('setModels', [...response]);

            });
        },
        getBrand({ commit, state }, brandDescription) {
            return api.getBrand(brandDescription).then(response => {

                commit('setBrand', response);

            });
        },
        getModel({ commit, state }, object) {
            return api.getModel(object.brandId, object.modelDescription).then(response => {

                commit('setModel', response);

            });
        },
        getModification({ commit, state }, object) {
            return api.getModification(object.modelId, object.modificationDescription).then(response => {

                commit('setModification', response);

            });
        },
        getSections({ commit, state }, object) {
            return api.getSections(
                object.brandDescription,
                object.modelDescription,
                object.modificationDescription
            ).then(response => {
                commit("setSections", response);
            });
        },
        getProducts({ commit, state }, object) {
            return api.getProducts(object.modificationId, object.sectionId, object.page).then(response => {

                    commit('setProducts', response);
                    commit('setProductCount', response.count);

                });
        },
        getSectionId({ commit, state }, object) {
            return api.getSectionById(object.sectionId, object.modificationId).then(response => {

                commit('setSection', response);

            });
        },
        getModificationId({ commit, state }, modificationId) {
            return api.getCarInfoByModificationId(modificationId).then(response => {

                commit('setCarInfo', response);

            });
        },
        getProductId({ commit, state }, id) {
            return api.getProductById(id).then(response => {

                commit("setProduct", response);

            });
        },
        getUserCart({ commit, state }, object) {
            return api.getUserCart(object.hash).then(response => {

                if(response.filter(item => item.basket.article == object.productId).length > 0) {
                    commit('setIsProductCart', true);
                } else {
                    commit('setIsProductCart', false);
                }

            });
        },
        getUserHash({ commit, state }) {
            return api.getUserHash();
        },
        addProductToBasket({ commit, state }, object) {
            return api.addItemToCart(
                object.modificationId,
                object.sectionId,
                object.productId,
                object.hash,
                object.price
            );
        }
    },
    getters: {
        getBrands(state) {
          return state.brands
        },
        getModels(state) {
            return state.models
        },
        getModifications(state) {
            return state.modifications
        },
        getSections(state) {
            return state.sections
        },
        getProducts(state) {
            return state.products;
        },

        getBrand(state) {
            return state.brand
        },
        getModel(state) {
            return state.model
        },
        getModification(state) {
          return state.modification
        },
        getSection(state) {
            return state.section;
        },
        getProduct(state) {
            return state.product;
        },

        getProductCount(state) {
            return state.productCount;
        },
        getIsProductCart(state) {
            return state.isProductCart;
        },



        getExpandedSectionIds(state) {
          return state.expandedSectionIds
        },
        getCarInfo(state) {
            return state.carInfo;
        },

        getBaseUrl(state) {
            return api.baseUrl;
        },
        getMobileMenuOpened(state) {
            return state.mobileMenuOpened
        },
        getPartsCategories(state) {
            return state.partsCategories
        },
    }
});

export default store
