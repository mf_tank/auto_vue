import axios from 'axios';

const http = axios.create({
  baseURL: "https://backend.zapchasti-express.ru/api/",
  headers: {
    "Content-Type": "application/json"
  }
});

http.interceptors.request.use((config) => {
  config.params = {};
  return config
});

http.interceptors.response.use(
  function(response) {
    return Promise.resolve(response.data);
  },
  function(error) {
    return Promise.reject(error);
  }
);

export default {
  getBrands() {
    return http.get('/manufacturers/passengers');
  },
  getModels(brandDescription) {
    return http.get(`/models/passengers?description=${brandDescription}`);
  },
  getModifications(brandDescription, modelDescription) {
    return http.get(`/passanger-cars/modifications?brand_description=${brandDescription}&model_description=${modelDescription}`);
  },
  getSections(brandDescription, modelDescription, modificationDescription) {
    return http.get(`/sections/all?brand_description=${brandDescription}&model_description=${modelDescription}&modification_description=${modificationDescription}`);
  },
  getProducts(modificationId, sectionId, page = '') {
    return http.get(`/details/list?modification_id=${modificationId}&section_id=${sectionId}${page ? '&page=' + page : ''}`);
  },


  getBrand($brandDescription) {
    return http.get(`/manufacturers/passenger?description=${$brandDescription}`);
  },
  getModel($brandId, $description) {
    return http.get(`/models/passenger?brand_id=${$brandId}&description=${$description}`);
  },
  getModification($modelId, $description) {
    return http.get(`/passanger-cars/modification?model_id=${$modelId}&description=${$description}`);
  },
  getSectionById(sectionId, modificationId) {
    return http.get(`/sections/view?id=${sectionId}&modification_id=${modificationId}`);
  },
  getProductById(id) {
    return http.get(`/details/view?id=${id}`);
  },

  getCarInfoByModificationId(modificationId) {
    return http.get(`/passanger-cars/view?id=${modificationId}`);
  },
  getUserHash() {
    return http.get('/basket/str')
  },
  getUserCart(userHash) {
    return http.get(`/basket/index?str=${userHash}`);
  },
  addItemToCart(modificationId, sectionId, article, userHash, price) {
    const params = new URLSearchParams();
    params.append('count', 1);
    params.append('article', article);
    params.append('modification_id', modificationId);
    params.append('section_id', sectionId);
    params.append('str', userHash);
    params.append('price', price);
    return http.post('/basket/create', params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
  removeBasket(id) {
    return http.delete(`/basket/delete/`+id);
  },
  sendForm(name, phone, text) {
    const params = new URLSearchParams();
    params.append('name', name);
    params.append('phone', phone);
    params.append('text', text);
    return http.post(`/feedback/create`, params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
  updateProductInCart(productIdInCart, count) {
    const params = new URLSearchParams();
    params.append('count', count);
    return http.put(`/basket/update/${productIdInCart}`, params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
  createOrder(userHash, name, phone) {
    const params = new URLSearchParams();
    params.append('str', userHash);
    params.append('name', name);
    params.append('phone', phone);

    return http.post(`/orders/create`, params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  },
  axios,
  baseUrl: 'https://backend.zapchasti-express.ru'
}

